const http = require("http");
const vars = require("./vars");
const app = require("./app");
const server = http.createServer(app.callback());
server.listen(vars.PORT)
