const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
const fs = require('fs');
const bcrypt = require('bcrypt');

readline.question("Please set a basic auth key to protect routes: ", (key) => {
    bcrypt.hash(key, 12, (err, hash) => {
        const config = {
            lock: hash,
        };
        fs.writeFile("./key.json", JSON.stringify(config), (err) => {
            if (err) throw err;
            console.log("Key has been set");
            process.exit(0);
        });
    });
});
