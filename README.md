# Dump Server
http service that opens protected route to add / update json documents in a server directory

# Installation
```
git clone https://gitlab.com/salih.four/dump-server
cd dump-server
npm install
npm run setkey
npm start
```

`npm run setkey` will prompt you to enter a key that will be used to restrict access to the service.  
By default, server will run on port 3000, you modify it in `app.js` in the `app.listen(3000)` function.
# Exposed routes
### `PUT: /protected/PATH`
Will dump request body into `/protected/PATH`. You may specify any `PATH`, it will be created if it does not exist.
### `GET: /protected/PATH`
Will obtain json response from `/protected/PATH`  


# Authorisation
Both endpoints require authorisation. You have to provide your key as password in basic auth header in your request, username is not ignored.
