const Koa = require('koa');
const app = new Koa();
const router = require('koa-router');
const bcrypt = require('bcrypt');
const bodyParser = require('koa-bodyparser');
const bauth = require('basic-auth');
const fs = require('fs').promises;
const path = require('path');
const fs_sync = require('fs');

const { lock } = require('./key.json');

async function handler(ctx, next){
    if (!ctx.request.url.match("/protected/*")){
        await next();
        return;
    }
    
    const user = bauth(ctx.request);
    const access = await bcrypt.compare(user.pass, lock);
    if (!access) {
        throw 401;
    }
    
    const filepath = path.join('./', ctx.request.url);
    const dir = path.dirname(filepath);
    if (!fs_sync.existsSync(dir)) {
        fs_sync.mkdirSync(dir, {recursive: true});
    }
    
    if (ctx.request.method == "PUT"){
        const content = JSON.stringify(ctx.request.body);
        await fs.writeFile(filepath, content);
        ctx.body = "Written";
    }
    
    if (ctx.request.method == "GET"){
        const content = await fs.readFile(filepath);
        ctx.response.type = 'application/json';
        ctx.body = JSON.parse(content);
    }
};

app.use(async (ctx, next) => {
    // Error handling
    try {
        await next();
    } catch (e) {
        if (e == 401) {
            ctx.status = 401;
            ctx.type = 'application/json';
            ctx.body = {msg: "Permission denied"};
            return;
        }

        ctx.status = 500;
        ctx.type = 'application/json';
        ctx.body = {msg: 'Some unknown error occured'};
        return;
        console.log(e);
    }
});

app.use(bodyParser());

app.use(handler); // Terminal, handler does not call anymore middlewares

app.listen(3000)


