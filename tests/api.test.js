const app = require("../app");
const request = require("supertest")(app.callback());
const accesskey = 'dummypass';

it("Stores and retrieves json", async () => {
    const json_string = JSON.stringify({msg: "Hello"});
    var response = await request.put("/protected/hello.js")
        .auth('', accesskey)
        .set('Content-Type', 'application/json')
        .send(json_string);
    expect(response.status).toBe(200);
    response = await request.get("/protected/hello.js")
        .auth('', accesskey);
    expect(response.status).toBe(200);
    expect(JSON.stringify(response.body)).toBe(json_string);
});

it("Prevents unauthorized view", async () => {
    var response = await request.get("/protected/hello.js");
    expect(response.status).toBe(401);
});

it("Prevents unauthorized edit", async () => {
    var res = await request.post("/protected/hello.js")
        .auth('', 'wrongkey')
        .set('Content-Type', 'application/json')
        .send(JSON.stringify({msg: "Hello"}));
    expect(res.status).toBe(401);
});
