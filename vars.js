const PORT = 3000;
const NODE_ENV = process.env.NODE_ENV;

if (!['production', 'development', 'test'].includes(process.env.NODE_ENV)) {
    console.log("Specify NODE_ENV environment var as one of production, testing, development"); 
    throw "Invalid NODE_ENV environment var";
}

module.exports = {
    PORT,
    NODE_ENV
}
